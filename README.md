# E-Book Template

## Format

- Cover: one picture
- Table of Contents

## Fonts

- OpenSans: http://www.1001fonts.com/open-sans-font.html
- SourceCodePro: http://www.1001fonts.com/source-code-pro-font.html
- EmojiOne: https://github.com/GuangchuangYu/emojifont/blob/master/inst/emoji_fonts/EmojiOne.ttf
- mplus1mn-regular-ascii-conums.ttf 🤔

## Theming

Look at: https://github.com/asciidoctor/asciidoctor-pdf/blob/v1.5.3/docs/theming-guide.adoc
